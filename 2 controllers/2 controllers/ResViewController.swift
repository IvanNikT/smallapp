//
//  second view controller.swift
//  2 controllers
//
//  Created by user on 27.02.2023.
//

import UIKit

class ResViewController: UIViewController {
    private let glavLabel2 = UILabel()
    private let nameLabel2 = UILabel()
    private let visualProgress = UIProgressView()
    private let procentLabel = UILabel()
    private let backButton = UIButton()
    public var srav: Float = 0.65

    public var firstName: String!
    public var secondName: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(red: 142/255, green: 205/255, blue: 225/255, alpha: 1)
        glav2Config()
        name2Config()
        visualProgressConfig()
        procentLabelConfig()
        backButtonConfig()
    }

    private func glav2Config() {
        glavLabel2.text = "Ваша совместимость"
        glavLabel2.font = UIFont(name:"Noteworthy", size: 23.0)
        view.addSubview(glavLabel2)
        glavLabel2.clipsToBounds = true
        glavLabel2.layer.cornerRadius = 5
        glavLabel2.textAlignment = .center
        glavLabel2.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            glavLabel2.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 20),
            glavLabel2.widthAnchor.constraint(equalToConstant: 300),
            glavLabel2.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        ])
    }

    private func name2Config() {
        nameLabel2.text = "\(firstName ?? "") и \(secondName ?? "" )"
        view.addSubview(nameLabel2)
        nameLabel2.clipsToBounds = true
        nameLabel2.numberOfLines = 0
        nameLabel2.layer.cornerRadius = 5
        nameLabel2.textAlignment = .center
        nameLabel2.font = UIFont(name: "HelveticaNeue-Bold", size: 20)
        nameLabel2.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            nameLabel2.topAnchor.constraint(equalTo: glavLabel2.bottomAnchor , constant: 30),
            nameLabel2.widthAnchor.constraint(equalToConstant: 300),
            nameLabel2.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        ])
    }

    private func procentLabelConfig() {
        procentLabel.text = "\(srav*100)%"
        view.addSubview(procentLabel)
        procentLabel.clipsToBounds = true
        procentLabel.layer.cornerRadius = 5
        procentLabel.textAlignment = .center
        procentLabel.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            procentLabel.topAnchor.constraint(equalTo: visualProgress.bottomAnchor, constant: 10),
            procentLabel.widthAnchor.constraint(equalToConstant: 100),
            procentLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        ])
    }

    private func visualProgressConfig() {
        visualProgress.setProgress(srav, animated: true)
        visualProgress.transform = visualProgress.transform.scaledBy(x: 1, y: 4)
        view.addSubview(visualProgress)
        visualProgress.clipsToBounds = true
        visualProgress.backgroundColor = .white
        visualProgress.progressTintColor = .red
        visualProgress.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            visualProgress.topAnchor.constraint(equalTo: nameLabel2.bottomAnchor, constant: 30),
            visualProgress.widthAnchor.constraint(equalToConstant: 200),
            visualProgress.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        ])
    }

    private func backButtonConfig() {
        view.addSubview(backButton)
        backButton.clipsToBounds = true
        backButton.backgroundColor = .black
        backButton.setTitleColor(.white, for: .normal)
        backButton.setTitle("назад", for: .normal)
        backButton.layer.cornerRadius = 5
        backButton.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            backButton.topAnchor.constraint(equalTo: procentLabel.bottomAnchor , constant: 30),
            backButton.widthAnchor.constraint(equalToConstant: 150),
            backButton.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        ])
        backButton.addTarget(self, action: #selector(pressBack), for: .touchUpInside)
    }

    @objc func pressBack() {
        dismiss(animated: true, completion: nil)
    }

}
