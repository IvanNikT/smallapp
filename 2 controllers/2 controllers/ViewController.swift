//
//  ViewController.swift
//  2 controllers
//
//  Created by user on 27.02.2023.
//

import UIKit

class ViewController: UIViewController {

    private let glavLabel = UILabel()
    private let nameLabel = UILabel()
    private let sernameLabel = UILabel()
    private let textNameField = UITextField()
    private let textSernameField = UITextField()
    private let resButton = UIButton()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(red: 142/255, green: 205/255, blue: 225/255, alpha: 1)
        glavConfig()
        nameConfig()
        sernameConfig()
        textNameFieldConfig()
        textSernameFieldConfig()
        resButtonConfig()
    }

    private func nameConfig() {
        nameLabel.text = "Твое имя"
        view.addSubview(nameLabel)
        nameLabel.clipsToBounds = true
        nameLabel.layer.cornerRadius = 5
        nameLabel.textAlignment = .left
        nameLabel.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            nameLabel.topAnchor.constraint(equalTo: glavLabel.bottomAnchor , constant: 30),
            nameLabel.widthAnchor.constraint(equalToConstant: 150),
            nameLabel.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor, constant: 30)
        ])
    }

    private func sernameConfig() {
        sernameLabel.text = "Имя партнера"
        view.addSubview(sernameLabel)
        sernameLabel.clipsToBounds = true
        sernameLabel.layer.cornerRadius = 5
        sernameLabel.textAlignment = .left
        sernameLabel.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            sernameLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor , constant: 30),
            sernameLabel.widthAnchor.constraint(equalToConstant: 150),
            sernameLabel.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor, constant: 30)
        ])
    }

    private func glavConfig() {
        glavLabel.text = "Совместимость имен"
        glavLabel.font = UIFont(name:"Noteworthy", size: 23.0)
        view.addSubview(glavLabel)
        glavLabel.clipsToBounds = true
        glavLabel.layer.cornerRadius = 5
        glavLabel.textAlignment = .center
        glavLabel.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            glavLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 20),
            glavLabel.widthAnchor.constraint(equalToConstant: 300),
            glavLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        ])
    }

    private func textNameFieldConfig() {
        view.addSubview(textNameField)
        textNameField.clipsToBounds = true
        textNameField.backgroundColor = .white
        textNameField.textColor = .black
        textNameField.layer.cornerRadius = 5
        textNameField.textAlignment = .center
        textNameField.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            textNameField.topAnchor.constraint(equalTo: glavLabel.bottomAnchor , constant: 30),
            textNameField.widthAnchor.constraint(equalToConstant: 150),
            textNameField.leftAnchor.constraint(equalTo: nameLabel.rightAnchor, constant: 10)
        ])
    }

    private func textSernameFieldConfig() {
        view.addSubview(textSernameField)
        textSernameField.clipsToBounds = true
        textSernameField.backgroundColor = .white
        textSernameField.textColor = .black
        textSernameField.layer.cornerRadius = 5
        textSernameField.textAlignment = .center
        textSernameField.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            textSernameField.topAnchor.constraint(equalTo: textNameField.bottomAnchor , constant: 30),
            textSernameField.widthAnchor.constraint(equalToConstant: 150),
            textSernameField.leftAnchor.constraint(equalTo: sernameLabel.rightAnchor, constant: 10)
        ])
    }

    private func resButtonConfig() {
        view.addSubview(resButton)
        resButton.clipsToBounds = true
        resButton.backgroundColor = .black
        resButton.setTitleColor(.white, for: .normal)
        resButton.setTitle("Проверить", for: .normal)
        resButton.layer.cornerRadius = 5
        resButton.addTarget(self, action: #selector(pressRes), for: .touchUpInside)
        resButton.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            resButton.topAnchor.constraint(equalTo: textSernameField.bottomAnchor , constant: 30),
            resButton.widthAnchor.constraint(equalToConstant: 150),
            resButton.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        ])
    }

    private func alertVC(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .default)
        alert.addAction(okAction)
        present(alert, animated: true)
    }

    private func resName(textN: String?) -> Float {
        var count: Float = 0.0
        guard textN != nil && textN != "" else { return 0.0 }
        for i in textN!.lowercased() {
            switch i {
            case "а","о", "и", "у", "е", "э", "ы", "я", "ю", "ё":
                count+=3
            default:
                count+=5
            }
        }
        return count
    }

    @objc func pressRes() {
        let rootVC = ResViewController()
        rootVC.firstName = textNameField.text
        rootVC.secondName = textSernameField.text
        var sumNames: String? = nil
        guard let name1 = textNameField.text, let name2 = textSernameField.text else { return }

        if name1 != "", name2 != "" {
                sumNames = name1 + name2
            } else {
                sumNames = nil
            }

        if let _ = sumNames {
            rootVC.srav = resName(textN: sumNames) / 1000
            let navVC = UINavigationController(rootViewController: rootVC)
            navVC.modalPresentationStyle = .fullScreen
            present(navVC, animated: true)
        } else {
                alertVC(title: "not names",
                        message: "Введите оба имени ☺️")
            }
    }
}
